# google-api-wrapper-laravel

Google API Library (PHP) for use at UNCG

# Google API Library - Laravel Wrapper

Contact: its-laravel-devs-l@uncg.edu

# Introduction

This package is a Laravel wrapper for the UNCG Google API PHP Library package, so that the Google API PHP Library can be used in Laravel apps.

> This is a **work in progress**. Not recommend for production apps just yet.

---

# Installation

1. `composer require 'uncgits/google-api-wrapper-laravel'`
2. *IF running Laravel 5.4 or below*: Add `Uncgits\GoogleApiLaravel\ServiceProvider::class,` to your `config/app.php` file
3. Run `php artisan vendor:publish --provider='Uncgits\GoogleApiLaravel\ServiceProvider'` - to publish the `google-api.php` config file
4. Set your environment credentials in your `.env` file, and set your configuration options in `config/google-api.php`

```
GOOGLE_APPLICATION_CREDENTIALS= relative path to your google credentials .json
GOOGLE_DOMAIN = the domain
GOOGLE_ADMIN_ACCOUNT = account with admin rights
GOOGLE_CUSTOMER_ID = the customer ID string for your account (required for some but not all calls)
```

## Dependencies

This package has dependencies on `uncgits/google-api-php-library` and `barryvdh/laravel-debugbar` (dev)

---

# Usage

## Basic Usage / Getting Started

In your code, assuming you've set your information/credentials properly in your `.env` file, you should be able to instantiate the `Uncgits\GoogleApiLaravel\GoogleApi` class, and then use any of its available methods (inherited from `uncgits/google-api-php-libary`) to make an API call.

> As of version 0.3, no flash messaging package is included. You are free to install your own - recommend `laracasts/flash` or something that implements a similar API.

---

# Version History

## 0.18

- improvements to `listGroupMembers()` method so that it will handle pagination through group membership if necessary

## 0.17

- Get Email

## 0.16.1

- Remove deprecated (and unintentional) usage of required parameters following optional ones in several methods. This mostly affects default 'me' values for several methods, which would not have been usable anyway, so it should not be backward breaking.

## 0.16

- Support for creating User Aliases

## 0.15

- Add support for push notification channels

## 0.14.1

- Add support for parameters in `listOrgUnits` call

## 0.14

- Features related to Org Units
- Features for updating POP/IMAP settings
- Fixes for not returning to super-admin subject after impersonating a user
- GZIP for results by default; add option in `.env` to turn this off

## 0.13

- Features related to email configuration

## 0.12

- Implement listing user email filters

## 0.11

- Implement list / delete forwarding address operations

## 0.10

- Implement batched API calls per https://developers.google.com/admin-sdk/directory/v1/guides/batch?hl=en:
    - Insert Group Members
    - Delete Group Members

## 0.9.2

- Fix for mis-labeled tokens method

## 0.9.1

- API Caching capability across all get/list requests

## 0.9

- API Cache, at a basic level.

## 0.8

- Trying unlimited pagination on `listGroups()` again.

## 0.7.1

- Adds option for email type, default `text/html`

## 0.7

- Email sending, using the `sendEmail()` method

## 0.6.2

- PSR-4 fixes

## 0.6.1

- standardize conversion of null value to empty string from getNextPageToken()

## 0.6

- Fixes / changes `listGroups()` method to consolidate params and options together for correct functionality

## 0.5.1

- Fixes config keys for HTTP proxy

> NOTE: if upgrading, make sure you add the new config keys into your `google-api.php` config file, and set them in your `.env` file if using a proxy.

## 0.5

- Changes to manually setting Google Credentials using `setAuthConfig()`, as the `useApplicationDefaultCredentials()` method was relying on `getenv()` under the hood... which does not play nicely with `php artisan route:cache`.

> NOTE: if upgrading, make sure you add the new config key into your `google-api.php` config file

## 0.4

- Change how we handle pagination for `listUsers()` since we can't get past memory limit issues...now returning one page plus the next page token.

## 0.3

- Removes `standaniels/flash` as a dependency. Optional to install a flash package.

## 0.2

- adds pagination to `listGroupMembers()`, `listUsers()`, `listGroups()`, and `listActivities()` methods.
- updates to formatting for PSR2

## 0.1.1

- Began working on code with Kevin. More to come soon.

## 0.1

- Initial release
