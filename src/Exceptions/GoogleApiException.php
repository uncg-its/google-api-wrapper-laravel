<?php

namespace Uncgits\GoogleApiLaravel\Exceptions;

use Exception;

class GoogleApiException extends Exception
{
    public $response;
    public $more;

    public function __construct($response, $more = '') {
        $this->response = $response;
        $this->more = $more;
    }
}
