<?php

namespace Uncgits\GoogleApiLaravel;

use Barryvdh\Debugbar\Facade as Debugbar;
use Google_Client;
use Google_Service_Directory;
use Google_Service_Directory_User;
use Google_Service_Directory_OrgUnit;
use Google_Service_Directory_Group;
use Google_Service_Directory_Member;
use Google_Service_Gmail;
use Google_Service_Groupssettings;
use Google_Service_Groupssettings_Groups;
use Google_Service_Exception;
use Google_Service_Gmail_ImapSettings;
use Google_Service_Gmail_Message;
use Google_Service_Reports;
use Swift_Message;
use Google_Service_Gmail_PopSettings;
use Google_Service_Directory_Channel;
use Google_Service_Directory_Alias;

class GoogleApi
{
    /*
    |--------------------------------------------------------------------------
    | Properties
    |--------------------------------------------------------------------------
    |
    | For abstraction these properties will hold the relevant environment and
    | auth info. Set these in a wrapper or whatever other framework you're
    | using.
    |
    */
    private $client;

    private $googleApiDomain;
    private $googleApiSubject;
    private $googleApiScopes;
    private $googleApiAccessType;
    private $googleApiCustomerId;

    protected $debugMode;

    protected $googleGroupSettingsPossibleValues = [
        'whoCanJoin'             => [
            'ALL_IN_DOMAIN_CAN_JOIN',
            'ANYONE_CAN_JOIN',
            'CAN_REQUEST_TO_JOIN',
            'INVITED_CAN_JOIN'
        ],
        'whoCanViewMembership'   => ['ALL_IN_DOMAIN_CAN_VIEW', 'ALL_MANAGERS_CAN_VIEW', 'ALL_MEMBERS_CAN_VIEW'],
        'whoCanViewGroup'        => [
            'ALL_IN_DOMAIN_CAN_VIEW',
            'ALL_MANAGERS_CAN_VIEW',
            'ALL_MEMBERS_CAN_VIEW',
            'ANYONE_CAN_VIEW'
        ],
        'whoCanInvite'           => ['ALL_MANAGERS_CAN_INVITE', 'ALL_MEMBERS_CAN_INVITE', 'NONE_CAN_INVITE'],
        'whoCanAdd'              => ['ALL_MEMBERS_CAN_ADD', 'ALL_MANAGERS_CAN_ADD', 'NONE_CAN_ADD'],
        'whoCanPostMessage'      => [
            'ALL_IN_DOMAIN_CAN_POST',
            'ALL_MANAGERS_CAN_POST',
            'ALL_MEMBERS_CAN_POST',
            'ANYONE_CAN_POST'
        ],
        'messageModerationLevel' => [
            'MODERATE_ALL_MESSAGES',
            'MODERATE_NEW_MEMBERS',
            'MODERATE_NONE',
            'MODERATE_NON_MEMBERS'
        ],
        'spamModerationLevel'    => ['ALLOW', 'MODERATE', 'SILENTLY_MODERATE', 'REJECT'],
        'replyTo'                => [
            'REPLY_TO_CUSTOM',
            'REPLY_TO_IGNORE',
            'REPLY_TO_LIST',
            'REPLY_TO_MANAGERS',
            'REPLY_TO_OWNER',
            'REPLY_TO_SENDER'
        ],
        'messageDisplayFont'     => ['DEFAULT_FONT', 'FIXED_WIDTH_FONT'],
        'whoCanLeaveGroup'       => ['ALL_MANAGERS_CAN_LEAVE', 'ALL_MEMBERS_CAN_LEAVE', 'NONE_CAN_LEAVE'],
        'whoCanContactOwner'     => [
            'ALL_IN_DOMAIN_CAN_CONTACT',
            'ALL_MANAGERS_CAN_CONTACT',
            'ALL_MEMBERS_CAN_CONTACT',
            'ANYONE_CAN_CONTACT'
        ],
    ];

    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    */


    /**
     * @param mixed $googleApiDomain
     */
    public function setGoogleApiDomain($googleApiDomain)
    {
        $this->googleApiDomain = $googleApiDomain;
    }


    /**
     * @param mixed $googleApiSubject
     */
    public function setGoogleApiSubject($googleApiSubject)
    {
        $this->googleApiSubject = $googleApiSubject;
    }

    /**
     * @param mixed $googleApiAccessType
     */
    public function setGoogleApiScopes($googleApiScopes)
    {
        $this->googleApiScopes = $googleApiScopes;
    }


    /**
     * @param mixed $googleApiAccessType
     */
    public function setGoogleApiAccessType($googleApiAccessType)
    {
        $this->googleApiAccessType = $googleApiAccessType;
    }

    public function setGoogleCustomerId($value)
    {
        $this->googleApiCustomerId = $value;
    }

    public function setGoogleClient()
    {
        $theClient = new Google_Client;
        $theClient->setAuthConfig(config('google-api.credential_file_path'));
        $theClient->setApplicationName(config('APP_NAME'));
        $theClient->setScopes($this->googleApiScopes);
        $theClient->setSubject($this->googleApiSubject);
        $theClient->setAccessType($this->googleApiAccessType);

        $clientParams = config('google-api.gzip') ?
            [
                'headers' => [
                    'Accept-Encoding' => 'deflate, gzip',
                    'User-Agent'      => config('app.name') . ' (gzip)'
                ]
            ] :
            [];

        if (config('google-api.http_proxy.enabled')) {
            $clientParams['proxy'] = config('google-api.http_proxy.host') . ':' . config('google-api.http_proxy.port');
            $clientParams['verify'] = false; // otherwise HTTPS requests will fail.
        }

        $httpClient = new \GuzzleHttp\Client($clientParams);
        $theClient->setHttpClient($httpClient);

        $this->client = $theClient;
    }

    public function getGoogleClient()
    {
        return $this->client;
    }

    protected function isCacheable(\Google_Service $service, $category, $method)
    {
        if (!config('google-api.cache.enabled')) {
            return false;
        }

        $cachedCallsForClient = config('google-api.cache.cacheable.' . get_class($service));
        if (is_null($cachedCallsForClient)) {
            return false;
        }

        if (!in_array($category . '.' . $method, $cachedCallsForClient)) {
            return false;
        }

        return true;
    }

    public function __construct()
    {
        $this->setGoogleApiDomain(config('google-api.domain'));

        $this->setGoogleApiSubject(config('google-api.subject'));
        $this->setGoogleApiScopes(config('google-api.scopes'));
        $this->setGoogleApiAccessType(config('google-api.access_type'));
        $this->setGoogleCustomerId(config('google-api.customer_id', ''));

        // debug mode (not enabled in production)
        $this->debugMode = (config('app.env') == 'production') ? false : config('google-api.debug_mode');

        $this->setGoogleClient();
    }

    public function call(\Google_Service $service, $category, $method, ...$args)
    {
        if (!$this->isCacheable($service, $category, $method)) {
            $result = $service->$category->$method(...$args);
            return $result;
        }

        $cacheKey = hash('sha3-512', get_class($service) . $category . $method . json_encode($args));
        if (!is_null($cached = \Cache::get($cacheKey))) {
            return unserialize($cached);
        }

        $result = $service->$category->$method(...$args);

        \Cache::put($cacheKey, serialize($result), now()->addMinutes(config('google-api.cache_minutes', 10)));

        return $result;
    }

    /**
     *  Users: list
     *
     * @param mixed $request
     *
     *  https://developers.google.com/admin-sdk/directory/v1/reference/users/list
     */

    public function listUsers($params = [], $pageToken = '')
    {
        if (!isset($params['maxResults'])) {
            $params['maxResults'] = 100;
        }

        if (!isset($params['customer']) && !isset($params['domain'])) {
            $params['domain'] = $this->googleApiDomain;
        }

        if (!empty($pageToken)) {
            $params['pageToken'] = $pageToken;
        }

        $service = new Google_Service_Directory($this->client);

        $response = $this->call($service, 'users', 'listUsers', $params);

        $users = $response->getUsers();

        return [
            'users'         => $users,
            'nextPageToken' => $response->getNextPageToken() ?? ''
        ];
    }

    /**
     * USER
     *
     * @param string $userKey
     *
     *  userKey may be primary email, id or other aliases
     *  https://developers.google.com/admin-sdk/directory/v1/reference/users/get
     */
    public function getUser($userKey, $params = [])
    {
        $service = new Google_Service_Directory($this->client);
        $user = $this->call($service, 'users', 'get', $userKey, $params);
        return $user;
    }

    public function createUser($user, $params = [])
    {
        // required props for $user: name.familyName, name.givenName, password, primaryEmail
        // optional props available at https://developers.google.com/admin-sdk/directory/v1/reference/users/insert
        $service = new Google_Service_Directory($this->client);
        $googleUser = new Google_Service_Directory_User($user);

        $response = $service->users->insert($googleUser, $params);
        return $response;
    }

    public function updateUser($userKey, $update)
    {
        $service = new Google_Service_Directory($this->client);
        $userUpdate = new Google_Service_Directory_User($update);

        $response = $service->users->update($userKey, $userUpdate);
        return $response;
    }

    public function deleteUser($userKey)
    {
        $service = new Google_Service_Directory($this->client);
        $response = $service->users->delete($userKey);

        return $response;
    }

    /**
     * PUSH NOTIFICATIONS
     *
     * https://developers.google.com/admin-sdk/directory/v1/guides/push
     *
     */

    public function watch($resource, $channelInfo = [], $optParams = [])
    {
        $service = new Google_Service_Directory($this->client);
        $channel = new Google_Service_Directory_Channel($channelInfo);
        return $service->$resource->watch($channel, $optParams);
    }

    public function stopWatch($channelId, $resourceId)
    {
        $service = new Google_Service_Directory($this->client);
        $channel = new Google_Service_Directory_Channel([
            'id'         => $channelId,
            'resourceId' => $resourceId,
        ]);
        return $service->channels->stop($channel);
    }

    /**
     *  USER ALIAS
     *
     * @param string $userKey
     *
     *  userKey may be primary email, id or other aliases
     *  https://developers.google.com/admin-sdk/directory/v1/reference/users/aliases/list
     */

    public function listUserAliases($userKey)
    {
        $service = new Google_Service_Directory($this->client);
        $aliasesList = $this->call($service, 'users_aliases', 'listUsersAliases', $userKey);
        return $aliasesList->getAliases();
    }

    public function createUserAlias($userKey, $email)
    {
        $service = new Google_Service_Directory($this->client);
        $alias = new Google_Service_Directory_Alias(['alias' => $email]);
        return $service->users_aliases->insert($userKey, $alias);
    }

    public function deleteUserAlias($userKey, $aliasToBeDeleted)
    {
        $service = new Google_Service_Directory($this->client);
        $response = $service->users_aliases->delete($userKey, $aliasToBeDeleted);
        return $response;
    }

    public function getUserGroupRole()
    {
    }


    /**
     *  USER TOKENS
     *
     * @params string $userKey, string $clientId
     *
     *  userKey may be primary email, id or other aliases
     *   https://www.googleapis.com/admin/directory/v1/users/userKey/tokens
     *
     *    return Token Resource
     *   {
     *   "kind": "admin#directory#token",
     *   "etag": etag,
     *   "clientId": string,
     *   "displayText": string,
     *   "anonymous": boolean,
     *   "nativeApp": boolean,
     *   "userKey": string,
     *   "scopes": [
     *   string
     *   ]
     *   }
     */
    public function listUserTokens($userKey)
    {
        $service = new Google_Service_Directory($this->client);

        $userTokensList = $this->call($service, 'tokens', 'listTokens', $userKey);
        return $userTokensList->getItems();
    }

    public function getUserToken($userKey, $clientId)
    {
        $service = new Google_Service_Directory($this->client);

        return $this->call($service, 'tokens', 'get', $userKey, $clientId);
    }

    public function deleteUserToken($userKey, $clientId)
    {
        $service = new Google_Service_Directory($this->client);

        return $service->tokens->delete($userKey, $clientId);
    }


    /**
     *   USER PHOTO
     *
     * @params string $userKey
     *         boolean $prepareForSrc
     *
     *  userKey may be primary email, id or other aliases
     *  https://developers.google.com/admin-sdk/directory/v1/reference/users/photos
     *
     *    return userPhoto Resource
     *   {
     *   "kind": "admin#directory#user#photo",
     *   "id": string,
     *   "etag": etag,
     *   "primaryEmail": string,
     *   "mimeType": string,
     *   "height": integer,
     *   "width": integer,
     *   "photoData": bytes
     *   }
     *
     *    if $prepareForSrc is TRUE then
     *      return a string appropriate for an img tag src value
     *
     */
    public function getUserPhoto($userKey, $prepareForSrc = false)
    {
        $service = new Google_Service_Directory($this->client);
        try {
            $userPhotoData = $service->users_photos->get($userKey);

            if ($prepareForSrc == true) {
                $userPhoto = Helpers::photoDataPrepareForSrc($userPhotoData);
            }
        } catch (Google_Service_Exception $e) {
            return false;
        }
        return $userPhoto;
    }


    /**
     *   GROUPS
     *
     * @options array of customer | domain | key (optional)
     *
     *  userKey may be primary email, id or other aliases
     *  https://developers.google.com/admin-sdk/directory/v1/reference/groups
     *
     *
     */
    public function listGroups($params = [], $pageToken = '')
    {
        $service = new Google_Service_Directory($this->client);

        // Either customer or domain must be provided
        if (isset($params['customer']) && !empty($params['customer'])) {
            $params['customer'] = $params['customer'];
        } elseif (isset($params['domain']) && !empty($params['domain'])) {
            $params['domain'] = $params['domain'];
        } else {
            $params['domain'] = $this->googleApiDomain;
        }

        if (isset($params['key']) && !empty($params['key'])) {
            $params['userKey'] = $params['key'];
            unset($params['key']);
        }

        if (!empty($pageToken)) {
            $params['pageToken'] = $pageToken;
        }

        $params['maxResults'] = $params['maxResults'] ?? 200;

        $groups = [];

        do {
            $response = $this->call($service, 'groups', 'listGroups', $params);
            $groups = array_merge($groups, (array)$response->getGroups());
            $params['pageToken'] = $response->getNextPageToken();
        } while (!is_null($response->getNextPageToken()));

        return $groups;
    }

    public function getGoogleGroupSettingsPossibleValues()
    {
        return $this->googleGroupSettingsPossibleValues;
    }

    public function getGroup($groupKey)
    {
        $service = new Google_Service_Directory($this->client);
        $group = $this->call($service, 'groups', 'get', $groupKey);
        return $group;
    }

    public function insertGroup($group_settings)
    {
        $service = new Google_Service_Directory($this->client);
        $newGroup = new Google_Service_Directory_Group($group_settings);
        $response = $service->groups->insert($newGroup);
        return $response;
    }

    public function patchGroup($groupKey, $patch)
    {
        $service = new Google_Service_Directory($this->client);
        $groupPatch = new Google_Service_Directory_Group($patch);
        $response = $service->groups->patch($groupKey, $groupPatch);
        return $response;
    }


    public function updateGroup($groupKey, $update)
    {
        $service = new Google_Service_Directory($this->client);
        $groupUpdate = new Google_Service_Directory_Group($update);
        $response = $service->groups->update($groupKey, $groupUpdate);
        return $response;
    }

    public function deleteGroupAlias($groupKey, $aliasToDelete)
    {
        $service = new Google_Service_Directory($this->client);
        $response = $service->groups_aliases->delete($groupKey, $aliasToDelete);
        return $response;
    }

    /*
     *  Group Settings API
     *  https://developers.google.com/admin-sdk/groups-settings/v1/reference/groups#methods
     *  Requires Group's email address. Group's ID will not work.
     *
     * {
     *  "kind": "groupsSettings#groups",
     *  "email": string,
     *  "name": string,
     *  "description": string,
     *  "whoCanAdd": string,
     *  "whoCanJoin": string,
     *  "whoCanViewMembership": string,
     *  "whoCanViewGroup": string,
     *  "whoCanInvite": string,
     *  "allowExternalMembers": string,
     *  "whoCanPostMessage": string,
     *  "allowWebPosting": string,
     *  "primaryLanguage": string,
     *  "maxMessageBytes": integer,
     *  "isArchived": string,
     *  "archiveOnly": string,
     *  "messageModerationLevel": string,
     *  "spamModerationLevel": string,
     *  "replyTo": string,
     *  "customReplyTo": string,
     *  "includeCustomFooter": string,
     *  "customFooterText": string,
     *  "sendMessageDenyNotification": string,
     *  "defaultMessageDenyNotificationText": string,
     *  "showInGroupDirectory": string,
     *  "allowGoogleCommunication": string,
     *  "membersCanPostAsTheGroup": string,
     *  "messageDisplayFont": string
     *  "includeInGlobalAddressList": string,
     *  "whoCanLeaveGroup": string,
     *  "whoCanContactOwner": string,
     *  }
     *
     */
    public function getGroupSettings($groupEmail, $alt = 'json')
    {
        $service = new Google_Service_Groupssettings($this->client);

        //  NOTE: Will return NULL values unless json specified via the optional parameters!
        $optParams = ['alt' => $alt];

        return $this->call($service, 'groups', 'get', $groupEmail, $optParams);
    }

    public function patchGroupSettings($groupEmail, $groupPatch)
    {
        $groupSettingsObject = new Google_Service_Groupssettings_Groups();
        foreach ($groupPatch as $key => $value) {
            $groupSettingsObject->$key($value);
        }

        $service = new Google_Service_Groupssettings($this->client);
        $response = $service->groups->patch($groupEmail, $groupSettingsObject);
        return $response;
    }


    /*
     *  Members
     *  https://developers.google.com/admin-sdk/directory/v1/reference/members
     *
     */
    public function listGroupMembers($groupKey, $options = [], $nextPageToken = '')
    {
        if (!isset($options['maxResults'])) {
            $options['maxResults'] = 200; // max from Google
        }

        $service = new Google_Service_Directory($this->client);

        // if we are provided a specific page token, get that page.
        if (!empty($nextPageToken)) {
            $options['nextPageToken'] = $nextPageToken;
            $response = $this->call($service, 'members', 'listMembers', $groupKey, $options);
            return [
                'groupMembers'  => $response->getMembers(),
                'nextPageToken' => $response->getNextPageToken() ?? '',
            ];
        }

        // if no page token is provided, then we get all members
        $members = [];
        do {
            $response = $this->call($service, 'members', 'listMembers', $groupKey, $options);
            $members = array_merge($members, $response->getMembers());
            if (isset($response->nextPageToken)) {
                $options['pageToken'] = $response->nextPageToken ?: null;
            } else {
                unset($options['pageToken']);
            }
        } while (isset($options['pageToken']));

        return $members;
    }

    public function getGroupMember($groupKey, $memberKey)
    {
        $service = new Google_Service_Directory($this->client);

        return $this->call($service, 'members', 'get', $groupKey, $memberKey);
    }

    public function hasGroupMember($groupKey, $memberKey)
    {
        $service = new Google_Service_Directory($this->client);

        return $this->call($service, 'members', 'hasMember', $groupKey, $memberKey);
    }


    public function insertGroupMember($groupKey, $userEmail, $userGroupRole)
    {
        $groupMember = new Google_Service_Directory_Member();
        $groupMember->setEmail($userEmail);
        $groupMember->setRole($userGroupRole);

        $service = new Google_Service_Directory($this->client);
        $response = $service->members->insert($groupKey, $groupMember);
        return $response;
    }

    public function batchInsertGroupMembers($groupKey, $userEmails, $userGroupRole)
    {
        // chunk into 1000s
        $chunks = collect($userEmails)->chunk(1000);

        $this->client->setUseBatch(true);

        $service = new Google_Service_Directory($this->client);
        $batch = $service->createBatch();

        $results = $chunks->map(function ($emails) use ($batch, $groupKey, $userGroupRole, $service) {
            collect($emails)->each(function ($email) use ($batch, $groupKey, $userGroupRole, $service) {
                $groupMember = new Google_Service_Directory_Member([
                    'email' => $email,
                    'role'  => $userGroupRole
                ]);

                $batch->add($service->members->insert($groupKey, $groupMember), $email);
            });

            return $batch->execute();
        });

        return $results;
    }

    public function patchGroupMember($groupKey, $memberKey, $memberPatch)
    {
        $memberSettingsObject = new Google_Service_Directory_Member();
        foreach ($memberPatch as $key => $value) {
            $memberSettingsObject->$key($value);
        }

        $service = new Google_Service_Directory($this->client);
        $response = $service->members->patch($groupKey, $memberKey, $memberSettingsObject);
        return $response;
    }

    public function updateGroupMember($groupKey, $memberKey, $userGroupRole)
    {
        $groupMember = new Google_Service_Directory_Member();
        $groupMember->setRole($userGroupRole);

        $service = new Google_Service_Directory($this->client);
        $response = $service->members->update($groupKey, $memberKey, $groupMember);
        return $response;
    }

    public function deleteGroupMember($groupKey, $userKey)
    {
        $service = new Google_Service_Directory($this->client);
        $response = $service->members->delete($groupKey, $userKey);
        return $response;
    }

    public function batchDeleteGroupMembers($groupKey, $userEmails, $userGroupRole)
    {
        // chunk into 1000s
        $chunks = collect($userEmails)->chunk(1000);

        $this->client->setUseBatch(true);

        $service = new Google_Service_Directory($this->client);
        $batch = $service->createBatch();

        $results = $chunks->map(function ($emails) use ($batch, $groupKey, $userGroupRole, $service) {
            collect($emails)->each(function ($email) use ($batch, $groupKey, $userGroupRole, $service) {
                $batch->add($service->members->delete($groupKey, $email), $email);
            });

            return $batch->execute();
        });

        return $results;
    }


    /*
     *  REPORTING RELATED
     *  https://developers.google.com/admin-sdk/reports/v1/get-start/getting-started
     *
     */

    public function listActivities($userKey = "all", $applicationName = "login", $optParams = [])
    {
        $service = new Google_Service_Reports($this->client);

        $activities = [];
        do {
            $response = $this->call($service, 'activities', 'listActivities', $userKey, $applicationName, $optParams);
            $activities = array_merge($activities, (array)$response->getItems());
            if (isset($response->nextPageToken)) {
                $optParams['pageToken'] = $response->nextPageToken ?: null;
            } else {
                unset($optParams['pageToken']);
            }
        } while (isset($optParams['pageToken']));

        return $response;
    }

    // send email

    public function sendEmail($to, $from, $subject, $body, $asUser = 'me', $type = 'text/html')
    {
        if ($asUser !== 'me') {
            $this->client->setSubject($asUser);
        }

        // create MIME mail using SwiftMailer, encode as base64
        $service = new Google_Service_Gmail($this->client);
        $mailer = $service->users_messages;

        $message = (new Swift_Message($subject, $body, $type, 'utf-8'))
            ->setFrom($from)
            ->setSender($from)
            ->setTo($to);

        // $msg_base64 = (new \Swift_Mime_ContentEncoder_Base64ContentEncoder())
        //     ->encodeString($message->toString());

        $msg_base64 = rtrim(strtr(base64_encode($message->toString()), '+/', '-_'), '=');

        $sent = new Google_Service_Gmail_Message();
        $sent->setRaw($msg_base64);
        $sent = $mailer->send($asUser, $sent);

        if ($asUser !== 'me') {
            $this->client->setSubject(config('google-api.subject'));
        }

        return $sent;
    }

    public function getEmailProfile($userId = 'me')
    {
        $service = new Google_Service_Gmail($this->client);
        if ($userId !== 'me') {
            $this->client->setSubject($userId);
        }
        $result = $service->users->getProfile($userId);
        if ($userId !== 'me') {
            $this->client->setSubject(config('google-api.subject'));
        }
        return $result;
    }

    public function getEmail($user, $id)
    {
        $service = new Google_Service_Gmail($this->client);
        $mailer = $service->users_messages;

        return $mailer->get($user, $id);
    }

    // list email forwards
    public function listEmailForwards($userId = 'me')
    {
        $service = new Google_Service_Gmail($this->client);
        if ($userId !== 'me') {
            $this->client->setSubject($userId);
        }
        $result = $service->users_settings_forwardingAddresses->listUsersSettingsForwardingAddresses($userId);
        if ($userId !== 'me') {
            $this->client->setSubject(config('google-api.subject'));
        }
        return $result;
    }

    // delete email forward
    public function deleteForward($userId, $forwardingEmail)
    {
        $service = new Google_Service_Gmail($this->client);
        $this->client->setSubject($userId);

        $result = $service->users_settings_forwardingAddresses->delete($userId, $forwardingEmail);
        $this->client->setSubject(config('google-api.subject'));

        return $result;
    }

    public function getEmailAutoForward($userId = 'me')
    {
        $service = new Google_Service_Gmail($this->client);
        if ($userId !== 'me') {
            $this->client->setSubject($userId);
        }
        $result = $service->users_settings->getAutoForwarding($userId);
        if ($userId !== 'me') {
            $this->client->setSubject(config('google-api.subject'));
        }
        return $result;
    }

    // list filters
    public function listEmailFilters($userId = 'me')
    {
        $service = new Google_Service_Gmail($this->client);
        if ($userId !== 'me') {
            $this->client->setSubject($userId);
        }
        $result = $service->users_settings_filters->listUsersSettingsFilters($userId);
        if ($userId !== 'me') {
            $this->client->setSubject(config('google-api.subject'));
        }
        return $result;
    }

    public function listEmailLabels($userId = 'me')
    {
        $service = new Google_Service_Gmail($this->client);
        if ($userId !== 'me') {
            $this->client->setSubject($userId);
        }
        $result = $service->users_labels->listUsersLabels($userId);
        if ($userId !== 'me') {
            $this->client->setSubject(config('google-api.subject'));
        }
        return $result;
    }

    public function getEmailLabel($userId, $labelId)
    {
        $service = new Google_Service_Gmail($this->client);
        $this->client->setSubject($userId);

        $result = $service->users_labels->get($userId, $labelId);
        $this->client->setSubject(config('google-api.subject'));

        return $result;
    }

    public function listEmailDelegates($userId = 'me')
    {
        $service = new Google_Service_Gmail($this->client);
        if ($userId !== 'me') {
            $this->client->setSubject($userId);
        }
        $result = $service->users_settings_delegates->listUsersSettingsDelegates($userId);
        if ($userId !== 'me') {
            $this->client->setSubject(config('google-api.subject'));
        }
        return $result;
    }


    public function getEmailVacation($userId = 'me')
    {
        $service = new Google_Service_Gmail($this->client);
        if ($userId !== 'me') {
            $this->client->setSubject($userId);
        }
        $result = $service->users_settings->getVacation($userId);
        if ($userId !== 'me') {
            $this->client->setSubject(config('google-api.subject'));
        }
        return $result;
    }

    public function getEmailSendAs($userId = 'me')
    {
        $service = new Google_Service_Gmail($this->client);
        if ($userId !== 'me') {
            $this->client->setSubject($userId);
        }
        $result = $service->users_settings_sendAs->listUsersSettingsSendAs($userId);
        if ($userId !== 'me') {
            $this->client->setSubject(config('google-api.subject'));
        }
        return $result;
    }

    public function getEmailLanguage($userId = 'me')
    {
        $service = new Google_Service_Gmail($this->client);
        if ($userId !== 'me') {
            $this->client->setSubject($userId);
        }
        $result = $service->users_settings->getLanguage($userId);
        if ($userId !== 'me') {
            $this->client->setSubject(config('google-api.subject'));
        }
        return $result;
    }

    public function getEmailPop($userId = 'me')
    {
        $service = new Google_Service_Gmail($this->client);
        if ($userId !== 'me') {
            $this->client->setSubject($userId);
        }
        $result = $service->users_settings->getPop($userId);
        if ($userId !== 'me') {
            $this->client->setSubject(config('google-api.subject'));
        }
        return $result;
    }

    public function updateEmailPop($userId, $settingsArray)
    {
        $service = new Google_Service_Gmail($this->client);
        $this->client->setSubject($userId);

        $settings = new Google_Service_Gmail_PopSettings($settingsArray);
        $result = $service->users_settings->updatePop($userId, $settings);
        $this->client->setSubject(config('google-api.subject'));

        return $result;
    }

    public function getEmailImap($userId = 'me')
    {
        $service = new Google_Service_Gmail($this->client);
        if ($userId !== 'me') {
            $this->client->setSubject($userId);
        }
        $result = $service->users_settings->getImap($userId);
        if ($userId !== 'me') {
            $this->client->setSubject(config('google-api.subject'));
        }
        return $result;
    }

    public function updateEmailImap($userId, $settingsArray)
    {
        $service = new Google_Service_Gmail($this->client);
        $this->client->setSubject($userId);

        $settings = new Google_Service_Gmail_ImapSettings($settingsArray);
        $result = $service->users_settings->updateImap($userId, $settings);
        $this->client->setSubject(config('google-api.subject'));

        return $result;
    }

    public function listOrgUnits($parameters = [])
    {
        $service = new Google_Service_Directory($this->client);
        return $service->orgunits->listOrgunits($this->googleApiCustomerId, $parameters);
    }

    public function getOrgUnit($idOrPath)
    {
        $service = new Google_Service_Directory($this->client);
        return $service->orgunits->get($this->googleApiCustomerId, $idOrPath);
    }

    public function createOrgUnit($orgunit)
    {
        // required props for $orgunit: name, parentOrgUnitPath OR parentOrgUnitId
        // optional props available at https://developers.google.com/admin-sdk/directory/reference/rest/v1/orgunits#OrgUnit
        $service = new Google_Service_Directory($this->client);
        $orgunit = new Google_Service_Directory_OrgUnit($orgunit);

        return $service->orgunits->insert($this->googleApiCustomerId, $orgunit);
    }

    public function updateOrgUnit($idOrPath, $update)
    {
        $service = new Google_Service_Directory($this->client);
        $orgunitUpdate = new Google_Service_Directory_OrgUnit($update);

        return $service->orgunits->update($this->googleApiCustomerId, $idOrPath, $orgunitUpdate);
    }

    public function deleteOrgUnit($idOrPath)
    {
        $service = new Google_Service_Directory($this->client);
        return $service->orgunits->delete($this->googleApiCustomerId, $idOrPath);
    }

    // debug helpers

    protected function debugMessage($message, $label = null)
    {
        if ($this->debugMode) {
            if (!is_null($label)) {
                Debugbar::addMessage($message, $label);
            } else {
                Debugbar::addMessage($message);
            }
        }
    }

    protected function debugInfo($object)
    {
        if ($this->debugMode) {
            Debugbar::info($object);
        }
    }

    protected function debugStartMeasure($key, $label = '')
    {
        if ($this->debugMode) {
            Debugbar::startMeasure($key, $label);
        }
    }

    protected function debugStopMeasure($key)
    {
        if ($this->debugMode) {
            Debugbar::stopMeasure($key);
        }
    }
}
