<?php

namespace Uncgits\GoogleApiLaravel;

class Helpers {


    /**
     * photoDataPrepareForSrc() -
     * converts the userPhoto mimeType and web-safe base64 photoData into the value appropriate for an img tags src value.
     *
     * @param array
     * @return string
     */
    public static function photoDataPrepareForSrc($userPhoto) {
        $imageSource = "data:" .$userPhoto['mimeType'] . ";";

        $userPhoto['photoData'] = str_replace("_","/", $userPhoto['photoData']);
        $userPhoto['photoData'] = str_replace("-","+", $userPhoto['photoData']);

        $imageSource .="base64," .$userPhoto['photoData'];

        return $imageSource;
    }

    public static function getEmailComponents($email) {

        $parts = explode('@', $email);
        $emailComponents = [
            'local' =>$parts[0],
            'domain' =>$parts[1]
        ];

        return $emailComponents;
    }


    public static function getGoogleGroupWebUrl($email){

        $emailComponents = Helpers::getEmailComponents($email);
        $googleGroupWebURL = "https://groups.google.com/a/" . $emailComponents['domain'] . "/forum/#!forum/" . $emailComponents['local'];

        return $googleGroupWebURL;
    }
}