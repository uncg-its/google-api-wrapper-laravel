<?php

return [

    //  DOMAIN
    'domain'  => env('GOOGLE_DOMAIN'),

    //  SCOPES
    /*  Uncomment or Add the ones you need
     *   See: https://developers.google.com/identity/protocols/googlescopes
     *  NOTE: Be sure to not have the last one be followed by a comma.
     *
     *  NOTE: There are different ways to provide scopes. One is by URL.
     *  Another is via name. For example
     *   Google_Service_Groupssettings::APPS_GROUPS_SETTINGS
     *
     *
     */
    'scopes'=> [
        'https://www.googleapis.com/auth/admin.directory.user',
        'https://www.googleapis.com/auth/admin.directory.user.security',
        'https://www.googleapis.com/auth/admin.directory.group',
        'https://www.googleapis.com/auth/admin.directory.group.readonly',
        'https://www.googleapis.com/auth/admin.directory.group.member',
        'https://www.googleapis.com/auth/apps.groups.settings',
        //'https://www.googleapis.com/auth/gmail.readonly',
        //'https://www.googleapis.com/auth/gmail.modify',
        //'https://apps-apis.google.com/a/feeds/emailsettings/2.0/',
        //'https://docs.google.com/feeds',
        //'https://spreadsheets.google.com/feeds',
        //'https://www.google.com/m8/feeds/',
        //'https://www.googleapis.com/auth/plus.login',
        //'https://www.googleapis.com/auth/calendar',
        'https://www.googleapis.com/auth/admin.reports.audit.readonly',
        'https://www.googleapis.com/auth/admin.reports.usage.readonly'
    ],

    // CREDENTIALS
    'subject'              => env('GOOGLE_ADMIN_ACCOUNT'),
    'credential_file_path' => env('GOOGLE_APPLICATION_CREDENTIALS'),

    // PROXY
    'http_proxy' => [
        'enabled' => env('GOOGLE_API_USE_PROXY', false) == 'true',
        'host'    => env('GOOGLE_API_PROXY_HOST', null),
        'port'    => env('GOOGLE_API_PROXY_PORT', null),
    ],

    //  ACCESS TYPE
    'access_type'  => env('GOOGLE_ACCESS_TYPE'),

    // SETTINGS
    'notification_mode' => ['flash','log'], // flash, log, or empty array for none
    'debug_mode'        => false, // true - extra logging in Barryvdh Debugbar (required)
    'gzip'              => env('GOOGLE_API_GZIP', true) == 'true',

     // CACHING
    'cache' => [
        'enabled'     => env('GOOGLE_API_CACHE_ENABLED', false),
        'ttl_minutes' => env('GOOGLE_API_CACHE_TTL_MINUTES', 10),
        'cacheable'   => [
            'Google_Service_Directory' => [
                // 'groups.listGroups',
                // 'groups.getGroup'
            ],
        ]
    ],

];
