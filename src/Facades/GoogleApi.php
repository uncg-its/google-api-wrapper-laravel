<?php

namespace Uncgits\GoogleApiLaravel\Facades;

use Illuminate\Support\Facades\Facade;

class GoogleApi extends Facade {

    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'GoogleApi'; // the IoC binding.
    }

}